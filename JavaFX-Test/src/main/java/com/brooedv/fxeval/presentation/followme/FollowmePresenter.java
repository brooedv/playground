package com.brooedv.fxeval.presentation.followme;

/*
 * #%L
 * igniter
 * %%
 * Copyright (C) 2013 - 2014 Adam Bien
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.brooedv.fxeval.business.flightcontrol.boundary.Tower;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javax.inject.Inject;

/**
 *
 * @author adam-bien.com
 */
public class FollowmePresenter implements Initializable {

    @FXML
    Label message;
    @FXML
    Label m;
    @FXML
    Button exit;
    @Inject
    Tower tower;

    @Inject
    private String prefix;

    @Inject
    private String happyEnding;

    @Inject
    private LocalDate date;

    private String theVeryEnd;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //fetched from followme.properties
        this.theVeryEnd = rb.getString("theEnd");
    }

    public void launch() {
        message.setText("Date: " + date + " -> " + prefix + tower.readyToTakeoff() + happyEnding + theVeryEnd
        );
    }
    public void launch2() {
        message.setText("MÖÖhhhhhhhhh");
        if(m.getText().equalsIgnoreCase("x")){
        	m.setText("U");
        	exit.setVisible(false);
        }
        else{
        	m.setText("X");
        	exit.setVisible(true);
        }
    }

    public void exit() {
        System.exit(0);
    }
}
