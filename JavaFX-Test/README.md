# Testcase for JavaFX8

[JavaFX8](http://docs.oracle.com/javase/8/javase-clienttechnologies.htm)

Aspects I want to include/cover:

* The Business-Case - Building fast business-UIs - Typical Controls
    * Tables
    * Calendar
    * TreeTable
* Based on Adam Biens [Afterburner.fx](http://afterburner.adam-bien.com)
* Another Chance for IntelliJ after Years
* UI-Design done with Oracles SceneBuilder 2.0
* Maven / Git


* [DataBinding](http://docs.oracle.com/javase/8/javafx/properties-binding-tutorial/title.htm)
    * per JavaBeanProperty oder JFXTras BeanPathAdapter
* I18N
* Field-Validation (Bean-Validation?)
    * e.g. FIN-Field / Only Date in future ...
* Complex Model-Validation
* Converter
* Analyse 3rd Party: JFXtras, ControlsFX, JavaX UI Controls Sandbox
* Printing-API
* Dialog?

* Distribution ->[ ['creating-maven-repository-on-shared-hosting'](http://piotrnowicki.com/2012/10/creating-maven-repository-on-shared-hosting) ]
